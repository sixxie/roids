#!/usr/bin/perl

use Math::Trig;
use POSIX;

my $angles = 32;
my $speed = 64;

print "vtable\n";

for my $i (0..$angles-1) {
	my $mul = ($i * 2.0) / $angles;
	my $rad = $mul * pi;
	my $dx = POSIX::floor(($speed * sin($rad)) + 0.5);
	my $dy = POSIX::floor((-$speed * cos($rad)) + 0.5);
	$dx = 65536 + $dx if ($dx < 0);
	$dy = 65536 + $dy if ($dy < 0);
	printf "\tfdb\t\$\%04x,\$\%04x\t; \%.2fpi\n", $dx, $dy, $mul;
}
