#!/usr/bin/perl

use POSIX;

print "sqr\n";
for my $i (0..39) {
	my $sqr = $i*$i;
	print "\tfdb\t" if (($i & 7) == 0);
	print "," if (($i & 7) != 0);
	printf "\$\%04x", $sqr;
	print "\n" if (($i & 7) == 7);
}
print "\n";
