#!/usr/bin/perl

use Math::Trig;
use POSIX;

my $angles = 32;
my $scale = 0.25;

my %graphics = (

	ship => [
		[ 0, -14 ],
		[ 8, 10 ],
		[ 0, 6 ],
		[ -8, 10 ],
		[ 0, -14 ],
	],

	roid0 => [
		[ -16, -32 ],
		[ 0, -24 ],
		[ 16, -32 ],
		[ 32, -16 ],
		[ 16, -8 ],
		[ 32, 8 ],
		[ 16, 32 ],
		[ -8, 24 ],
		[ -16, 32 ],
		[ -32, 16 ],
		[ -24, 0 ],
		[ -32, -16 ],
		[ -16, -32 ],
	],

	roid1 => [
		[ -32, -16 ],
		[ -8, -16 ],
		[ -16, -32 ],
		[ 8, -32 ],
		[ 32, -16 ],
		[ 32, -8 ],
		[ 8, 0 ],
		[ 32, 16 ],
		[ 16, 32 ],
		[ 8, 24 ],
		[ -16, 32 ],
		[ -32, 8 ],
		[ -32, -16 ],
	],

	roid2 => [
		[ -8, -32 ],
		[ 16, -32 ],
		[ 32, -8 ],
		[ 32, 8 ],
		[ 16, 32 ],
		[ 0, 32 ],
		[ 0, 8 ],
		[ -16, 32 ],
		[ -32, 8 ],
		[ -16, 0 ],
		[ -32, -8 ],
		[ -8, -32 ],
	],

	roid0a => [
		[ -16, -4 ],
		[ -8, -16 ],
		[ 12, -12 ],
		[ 16, 8 ],
		[ 0, 16 ],
		[ 0, 8 ],
		[ -8, 16 ],
		[ -16, 4 ],
		[ -8, 0 ],
		[ -16, -4],
	],

	roid1a => [
		[ -16, -8 ],
		[ -8, -16 ],
		[ 8, -16 ],
		[ 16, -8 ],
		[ 8, -4 ],
		[ 16, 4 ],
		[ 8, 16 ],
		[ -16, 12 ],
		[ -12, 0 ],
		[ -16, -8 ],
	],

	roid0aa => [
		[ -8, -4 ],
		[ -2, -8 ],
		[ 8, -4 ],
		[ 6, 8 ],
		[ -4, 8 ],
		[ -8, 4 ],
		[ -8, -4 ],
	],

);

for my $name (sort keys %graphics) {
	my $vlist = $graphics{$name};
	my $maxrs = 0.0;
	for my $i (0..$angles-1) {
		print "${name}_$i\n";
		print "; ";
		print (($i * 2.0) / $angles);
		print "\n";
		my $rad = ($i * 2.0 * pi) / $angles;
		my $x1 = $$vlist[0][0] * $scale;
		my $y1 = $$vlist[0][1] * $scale;
		for my $vertex (@{$vlist}[1..$#$vlist]) {
			my $x2 = $$vertex[0] * $scale;
			my $y2 = $$vertex[1] * $scale;
			my $xp1 = ($x1 * cos($rad)) - ($y1 * sin($rad));
			my $yp1 = ($x1 * sin($rad)) + ($y1 * cos($rad));
			my $xp2 = ($x2 * cos($rad)) - ($y2 * sin($rad));
			my $yp2 = ($x2 * sin($rad)) + ($y2 * cos($rad));
			my $rs1 = ($xp1*$xp1)+($yp1*$yp1);
			$maxrs = $rs1 if ($rs1 > $maxrs);
			$xp1 = POSIX::floor($xp1 + 0.5);
			$yp1 = POSIX::floor($yp1 + 0.5);
			$xp2 = POSIX::floor($xp2 + 0.5);
			$yp2 = POSIX::floor($yp2 + 0.5);
			my ($xx1, $yy1, $xx2, $yy2);
			if ($xp2 >= $xp1) {
				($xx1,$yy1) = ($xp1,$yp1);
				($xx2,$yy2) = ($xp2,$yp2);
			} else {
				($xx1,$yy1) = ($xp2,$yp2);
				($xx2,$yy2) = ($xp1,$yp1);
			}
			my $dx = $xx2 - $xx1;
			my $dy = $yy2 - $yy1;
			print "\tfcb\t$xx1, $yy1, $dx, $dy\n";
			$x1 = $x2;
			$y1 = $y2;
		}
	}
	$maxrs = POSIX::floor($maxrs);
	print "$name\t; maxrs=$maxrs\n";
	print "\tfcb\t$#$vlist\n";
	for my $i (0..$angles-1) {
		print "\tfdb\t${name}_$i\n";
	}
}
