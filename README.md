# Roids

An (unfinished) Asteroids-esque environment. All the vector offsets are
precalculated, but line drawing is done properly. Keys A and S allow rotation
of the ship, Shift accelerates, and Enter fires. You can shoot asteroids, but
can't die yet and there's no score. Speed is very variable and depends on the
number of objects on screen. 
