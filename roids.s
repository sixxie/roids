			include	"dragonhw.s"

			section "struct"

			org	0
object_angle		rmb	1
object_x		rmb	2
object_y		rmb	2
object_shape		rmb	2
object_radius2		rmb	2
object_dr		rmb	1
object_dx		rmb	2
object_dy		rmb	2
sizeof_object

			org	0
shot_timer		rmb	1
shot_x			rmb	2
shot_y			rmb	2
shot_dx			rmb	2
shot_dy			rmb	2
sizeof_shot

			org	0
shape_nedges		rmb	1
; one pointer to list of edges for each of 32 angles
shape_edges		rmb	32*2
sizeof_shape

			org	0
edge_xoff		rmb	1
edge_yoff		rmb	1
edge_dx			rmb	1
edge_dy			rmb	1
sizeof_edge

			org	0
undraw_pcount		rmb	1
undraw_dx		rmb	1
undraw_dy		rmb	1
undraw_error		rmb	1
undraw_jmp_offset	rmb	1
undraw_jmp_base		rmb	2
undraw_screen_addr	rmb	2
sizeof_undraw_record

			section "CODE"

fb_width	equ	32
fb_height	equ	192
fb_size		equ	fb_width*fb_height
fb0_base	equ	$0200
fb0_end		equ	fb0_base+fb_size
fb1_base	equ	$1a00
fb1_end		equ	fb1_base+fb_size

nshots		equ	4

; fb0 = page 1 (0001)
show_fb0	macro
		sta	reg_sam_f3c
		sta	reg_sam_f2c
		endm
; fb1 = page 13 (1101)
show_fb1	macro
		sta	reg_sam_f3s
		sta	reg_sam_f2s
		endm
; first switch needs extra bits initialised:
show_fb0_init	macro
		show_fb0
		sta	reg_sam_f1c
		sta	reg_sam_f0s
		endm

screen_x0	equ	0
screen_y0	equ	0
screen_x1	equ	255
screen_y1	equ	191
screen_height	equ	192

		org	$3200

main_dp		equ	(*>>8)
screen_base	rmb	2
line_count	rmb	1
angle		rmb	1
xoff		rmb	1
yoff		rmb	1
x0		rmb	1
y0		rmb	1
dx		rmb	1
dy		rmb	1
pcount		rmb	1
tmps		rmb	2
armed		rmb	1
tmp0		rmb	2

main
		orcc	#$50	; disable all cpu interrupts
		ldb	#$ff
		tfr	b,dp
		setdp	$ff

		; disable hsync interrupts
		ldd	#$3435
		sta	reg_pia0_cra
		; enable fsync interrupts
		stb	reg_pia0_crb

		; clear fb0
		clra
		clrb
		ldx	#fb0_base
1		std	,x++
		cmpx	#fb0_end
		blo	1B

		; 32 bytes per line
		sta	reg_sam_v0c
		sta	reg_sam_v1s
		sta	reg_sam_v2s
		show_fb0_init
		; VDG mode RG6
		lda	#$fc
		sta	reg_pia1_pdrb

		lda	#main_dp
		tfr	a,dp
		setdp	main_dp

mainloop

		; undraw lines on screen 1
		ldu	#fb1_end
		jsr	undraw_objects

		; draw objects on screen 1
		ldx	#fb1_base
		stx	screen_base
		jsr	draw_objects

		; wait for IRQ
		lda	reg_pia0_pdrb	; clear outstanding IRQ
1		lda	reg_pia0_crb
		bpl	1B

		show_fb1

		bsr	scan_keyboard
		jsr	move_objects

		; undraw lines on screen 0
		ldu	#fb0_end
		jsr	undraw_objects

		; draw objects on screen 0
		ldx	#fb0_base
		stx	screen_base
		jsr	draw_objects

		; wait for IRQ
		lda	reg_pia0_pdrb	; clear outstanding IRQ
1		lda	reg_pia0_crb
		bpl	1B

		show_fb0

		bsr	scan_keyboard
		jsr	move_objects

		jmp	mainloop

scan_keyboard
		clrb
		lda	#$fd
		sta	reg_pia0_pdrb
		lda	reg_pia0_pdra
		bita	#$04
		bne	scank0
		; 'a' pressed
		decb
scank0
		lda	#$f7
		sta	reg_pia0_pdrb
		lda	reg_pia0_pdra
		bita	#$10
		bne	scank1
		; 's' pressed
		incb
scank1
		stb	object0+object_dr
		lda	#$7f
		sta	reg_pia0_pdrb
		lda	reg_pia0_pdra
		bita	#$40
		bne	decelerate
		; 'shift' pressed
accelerate
		ldx	#vtable
		ldb	object0+object_angle
		lslb
		lslb
		abx

		ldd	object0+object_dx
		addd	,x
		cmpd	#$0500
		blt	10F
		ldd	#$0500
10		cmpd	#-$0500
		bgt	20F
		ldd	#-$0500
20		std	object0+object_dx

		ldd	object0+object_dy
		addd	2,x
		cmpd	#$0500
		blt	10F
		ldd	#$0500
10		cmpd	#-$0500
		bgt	20F
		ldd	#-$0500
20		std	object0+object_dy

		bra	scank2
		; 'shift' not pressed - decelerate

; dx = dx + dx/32, dy = dy + dy/32
; as initial |dx| should never be > $0700, don't need to worry about a
; after the first four shifts

decelerate
		ldd	object0+object_dx
		asra
		rorb
		asra
		rorb
		asra
		rorb
		asra
		rorb
		asrb
		beq	10F
		negb
		sex
		addd	object0+object_dx
10		std	object0+object_dx
		ldd	object0+object_dy
		asra
		rorb
		asra
		rorb
		asra
		rorb
		asra
		rorb
		asrb
		beq	10F
		negb
		sex
		addd	object0+object_dy
10		std	object0+object_dy

scank2
		lda	#$fe
		sta	reg_pia0_pdrb
		lda	reg_pia0_pdra
		bita	#$40
		bne	scank3
		; 'enter' pressed
		lda	armed
		bne	99F
fire
		ldu	#shot_list
10		lda	,u
		beq	100F
		leau	sizeof_shot,u
		cmpu	#shot_list_end
		blo	10B
99		rts
scank3
		clr	armed
		rts

100		lda	#32
		sta	shot_timer,u
		sta	armed
		ldx	#vtable
		ldb	object0+object_angle
		lslb
		lslb
		abx
		ldd	,x
		lslb
		rola
		addd	,x
		lslb
		rola
		lslb
		rola
		addd	object0+object_dx
		std	shot_dx,u
		ldd	2,x
		lslb
		rola
		addd	2,x
		lslb
		rola
		lslb
		rola
		addd	object0+object_dy
		std	shot_dy,u
		ldd	object0+object_x
		addd	shot_dx,u
		std	shot_x,u
		ldd	object0+object_y
		addd	shot_dy,u
		std	shot_y,u
		rts

; ----------------------------------------------------------------------------

; Move objects - apply velocity (dx,dy) to object position (x,y)
; x -> object list

move_objects
		ldx	#object_list
100		lda	object_angle,x
		bmi	980F
		adda	object_dr,x
		anda	#$1f
		sta	object_angle,x
		ldd	object_dx,x
		addd	object_x,x
		std	object_x,x
		ldd	object_dy,x
		bmi	110F
		addd	object_y,x
		cmpa	#screen_y1
		bls	120F
		clra
		bra	120F
110		addd	object_y,x
		cmpa	#screen_y1
		bls	120F
		lda	#screen_y1
120		std	object_y,x
980		leax	sizeof_object,x
		cmpx	#object_list_end
		blo	100B

		ldy	#sqr
		bsr	move_shots

move_shots	ldx	#shot_list
100		lda	shot_timer,x
		beq	990F
		deca
		sta	shot_timer,x

		ldd	shot_dx,x
		addd	shot_x,x
		std	shot_x,x

		ldd	shot_dy,x
		bmi	110F
		; dy is +ve
		addd	shot_y,x
		cmpa	#screen_y1
		bls	120F
		suba	#screen_height
		bra	120F
		; dy is -ve
110		addd	shot_y,x
		cmpa	#screen_y1
		bls	120F
		adda	#screen_height
120		std	shot_y,x

		ldu	#object_list+sizeof_object
200		lda	object_angle,u
		bmi	980F

		ldd	shot_x,x
		subd	object_x,u
		bpl	210F
		nega
210		cmpa	#40
		bhs	980F
		lsla
		ldd	a,y	; d=dx^2
		std	tmp0

		ldd	shot_y,x
		subd	object_y,u
		bpl	220F
		nega
220		cmpa	#40
		bhs	980F
		lsla
		ldd	a,y	; d=dy^2
		addd	tmp0	; d=(dx^2)+(dy^2)

		cmpd	object_radius2,u
		bhi	980F

		ldb	#$ff
		stb	object_angle,u
		clr	shot_timer,x

980		leau	sizeof_object,u
		cmpu	#object_list_end
		blo	200B

990		leax	sizeof_shot,x
		cmpx	#shot_list_end
		blo	100B
		rts

; ----------------------------------------------------------------------------

; Draw objects
; u -> object list

draw_objects
		ldu	#object_list
100
		ldb	object_angle,u
		bmi	980F

		; b = object_angle
		ldx	object_shape,u
		lda	,x+		; shape_nedges
		sta	line_count
		lslb
		ldy	b,x		; -> edge

		lda	object_x,u
		ldb	object_y,u
		std	xoff		; & yoff

200
		ldd	edge_xoff,y
		adda	xoff
		addb	yoff
		cmpb	#screen_y1	; y starts off-screen?
		bhi	skip_edge
		std	x0
		ldd	edge_dx,y	; & edge_dy
		std	dx		; & dy
		adda	x0
		bcs	skip_edge	; x wraps around?
		addb	y0
		cmpb	#screen_y1	; y finishes off-screen?
		bhi	skip_edge

		bsr	line

skip_edge
		leay	sizeof_edge,y
		dec	line_count
		bne	200B

980		leau	sizeof_object,u
		cmpu	#object_list_end
		blo	100B

draw_shots
		ldu	#shot_list
		ldy	#pixel
100		lda	shot_timer,u
		beq	990F

		ldx	screen_base
		lda	#32
		ldb	shot_y,u
		mul
		leax	d,x
		ldb	shot_x,u
		lsrb
		lsrb
		lsrb
		abx
		ldb	shot_x,u
		andb	#7
		ldb	b,y
		orb	,x
		stb	,x

990		leau	sizeof_shot,u
		cmpu	#shot_list_end
		blo	100B
		rts

pixel		fcb	$80,$40,$20,$10,$08,$04,$02,$01

; ----------------------------------------------------------------------------

; in: x0,y0,x1,y1 in dp set up
; destroyed: all but y,u

line
		pshs	u

		; calculate screen position
		ldx	screen_base
		lda	#32
		ldb	y0
		mul
		leax	d,x
		ldb	x0
		lsrb
		lsrb
		lsrb
		leau	b,x		; u = screen position

		; is dy positive or negative?
		ldx	#s7jmp
		ldb	dy
		bpl	1F
		negb
		stb	dy
		ldx	#s0jmp
1		; b = dy

		lda	dx
		; a = dx

		cmpb	dx
		bhi	set1

		; dx > dy
set0		inca
		sta	pcount
		deca
		lsra			; a = error = dx / 2
		coma
		ldb	x0
		andb	#7
		lslb
		abx
		ldb	,u
		jmp	[,x]

		; dy > dx
set1		incb
		stb	pcount
		decb
		lsrb			; b = error = dy / 2
		comb
		lda	x0
		anda	#7
		lsla
		adda	#16
		jmp	[a,x]

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

s0pixel		macro
		orb	#\1		; 2
		dec	pcount		; 6
		beq	\3		; 3
		adda	dy		; 4
		bcc	105F		; 3
		stb	,u		; 4 ?
		suba	dx		; 4 ?
		leau	\2,u		; 5 ?
		ldb	,u		; 4 ?
105		;			; total = 18 or 35 cycles
		endm

s0last		macro
		orb	#\1		; 2
		stb	,u		; 4
		dec	pcount		; 6
		beq	\3		; 3
		adda	dy		; 4
		bcc	105F		; 3
		suba	dx		; 4 ?
		leau	\2,u		; 5 ?
		jmp	\4		; 4+4 ?
105		leau	1,u		; 5 ??
		jmp	\4		; 4+4 ??
		endm			; total = 39 or 35 cycles

s1pixel		macro
100		lda	#\1		; 2
		ora	,u		; 4
		sta	,u		; 4
		dec	pcount		; 6
		beq	\3		; 3
		leau	\2,u		; 5
		addb	dx		; 4
		bcc	100B		; 3
		subb	dy		; 4 ?
		endm			; total = 31 or 35 cycles

899		stb	,u
900		puls	u,pc

s7c0pre		ldb	,u
s7c0		s0pixel	$80, 32, 899B
s7c1		s0pixel	$40, 32, 899B
s7c2		s0pixel	$20, 32, 899B
s7c3		s0pixel	$10, 32, 899B
s7c4		s0pixel	$08, 32, 899F
s7c5		s0pixel	$04, 32, 899F
s7c6		s0pixel	$02, 32, 899F
s7c7		s0last	$01, 33, 900F, s7c0pre

899		stb	,u
900		puls	u,pc

s6c0		s1pixel	$80, 32, 900B
s6c1		s1pixel	$40, 32, 900B
s6c2		s1pixel	$20, 32, 900B
s6c3		s1pixel	$10, 32, 900B
s6c4		s1pixel	$08, 32, 900F
s6c5		s1pixel	$04, 32, 900F
s6c6		s1pixel	$02, 32, 900F
s6c7		s1pixel	$01, 32, 900F
		leau	1,u
		jmp	s6c0

899		stb	,u
900		puls	u,pc

s0c0pre		ldb	,u
s0c0		s0pixel	$80, -32, 899B
s0c1		s0pixel	$40, -32, 899B
s0c2		s0pixel	$20, -32, 899B
s0c3		s0pixel	$10, -32, 899B
s0c4		s0pixel	$08, -32, 899F
s0c5		s0pixel	$04, -32, 899F
s0c6		s0pixel	$02, -32, 899F
s0c7		s0last	$01, -31, 900F, s0c0pre

899		stb	,u
900		puls	u,pc

s1c0		s1pixel	$80, -32, 900B
s1c1		s1pixel	$40, -32, 900B
s1c2		s1pixel	$20, -32, 900B
s1c3		s1pixel	$10, -32, 900B
s1c4		s1pixel	$08, -32, 900F
s1c5		s1pixel	$04, -32, 900F
s1c6		s1pixel	$02, -32, 900F
s1c7		s1pixel	$01, -32, 900F
		leau	1,u
		jmp	s1c0

900		puls	u,pc

; ----------------------------------------------------------------------------

undraw_objects	pshs	dp
		sts	tmps

		ldd	#$4000
		ldx	#$0000
		leay	,x
		leas	,x
		tfr	b,dp

10		pshu	b,dp,x,y,s
		pshu	b,dp,x,y,s
		pshu	b,dp,x,y,s
		pshu	b,dp,x,y,s
		pshu	b,dp,x,y,s
		pshu	b,dp,x,y,s
		pshu	b,dp,x,y,s
		pshu	b,dp,x,y,s
		pshu	b,dp,x,y,s
		pshu	b,dp,x,y,s
		pshu	b,dp,x,y,s
		pshu	b,dp,x,y,s
		deca
		bne	10B

		lds	>tmps
		puls	dp,pc

; ----------------------------------------------------------------------------

s7jmp		fdb	s7c0, s7c1, s7c2, s7c3, s7c4, s7c5, s7c6, s7c7
s6jmp		fdb	s6c0, s6c1, s6c2, s6c3, s6c4, s6c5, s6c6, s6c7
s0jmp		fdb	s0c0, s0c1, s0c2, s0c3, s0c4, s0c5, s0c6, s0c7
s1jmp		fdb	s1c0, s1c1, s1c2, s1c3, s1c4, s1c5, s1c6, s1c7

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

object		macro		; angle, x, y, shape, radius^2, dr, dx, dy
		fcb		\1	; angle
		fdb		\2*256,\3*256	; x,y
		fdb		\4	; shape
		fdb		\5	; radius^2
		fcb		\6	; dr
		fdb		\7*256,\8*256	; dx,dy
		endm

object_list

object0
		; player's ship
		object		 0, 128, 96, ship,    12,  0,  0, 0

		; large asteroids
		object		 2, 100, 23, roid0,   80,  1,  3, 1
		object		 4, 150, 43, roid1,   80, -1, -3,-1
		object		 6,  10, 10, roid2,   80, -1, -3, 2

		; medium asteroids
		object		 8, 200,100, roid0a,  20,  1, -3,-2
		object		 9, 210,130, roid1a,  20, -1,  3, 2

		; small asteroids
		object		11,  22, 10, roid0aa,  6,  1,  1, 3
		object		13,  50, 13, roid0aa,  6,  1,  3,-1
		object		15,  50, 20, roid0aa,  6,  1,  2,-1
		object		17, 192, 32, roid0aa,  6, -1, -2,-1
		object		19,  64, 48, roid0aa,  6,  1,  2, 1
		object		21, 144, 72, roid0aa,  6, -1,  1, 1
		object		23,  16, 88, roid0aa,  6,  1, -1, 1
		object		25,  92, 92, roid0aa,  6, -1, -2, 1
		object		27,  24,120, roid0aa,  6,  1,  3,-2
		object		29, 192,144, roid0aa,  6, -1,  1,-2
		object		31,  64,160, roid0aa,  6,  1,  1, 2

object_list_end

shot_list
		rzb	nshots*sizeof_shot
shot_list_end

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		include	"graphics.s"
		include	"sqrtable.s"
		include	"vtable.s"

; ----------------------------------------------------------------------------

