ASM6809 = asm6809
BIN2CAS = bin2cas.pl

.PHONY: all
all: roids.bin roids.cas

%.bin: %.s
	$(ASM6809) -D -e main -l $(<:.s=.lis) -o $@ $<

%.cas %.wav: %.bin
	$(BIN2CAS) $(B2CFLAGS) $(B2CFLAGS_ADD) -o $@ $<

roids.bin: vtable.s graphics.s sqrtable.s

roids.cas roids.wav: B2CFLAGS_ADD = --autorun -n "ROIDS" --eof-data --dzip --fast -D

vtable.s: ./vtable.pl
	./vtable.pl > $@

graphics.s: ./graphics.pl
	./graphics.pl > $@

sqrtable.s: ./sqrtable.pl
	./sqrtable.pl > $@

.PHONY: clean
clean:
	rm -f roids.lis vtable.s graphics.s sqrtable.s
	rm -f roids.bin roids.cas roids.wav
